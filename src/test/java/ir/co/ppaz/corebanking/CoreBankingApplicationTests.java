package ir.co.ppaz.corebanking;

import ir.co.ppaz.corebanking.enums.GenderEnum;
import ir.co.ppaz.corebanking.model.*;
import ir.co.ppaz.corebanking.service.CustomerService;
import ir.co.ppaz.corebanking.service.DepositService;
import ir.co.ppaz.corebanking.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CoreBankingApplicationTests {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    CustomerService customerService;

    @Autowired
    DepositService depositService;

    @Test
    void contextLoads() {
    }

//    @Test
//    void seedDefaultEmployee(){
//        if(employeeService.findAllEmployee().isEmpty()){
//            employeeService.save(Employee.builder()
//                    .firstName("Soroush")
//                    .lastName("Mohammadi")
//                    .gender(GenderEnum.Male)
//                    .username("123")
//                    .password("123456")
//                    .build());
//        }
//    }
//
//    @Test
//    void seedCustomer(){
//        if(customerService.getAllCustomers().isEmpty()){
//            customerService.saveReal(RealCustomer.builder()
//                    .birthDate("1369/05/16")
//                    .family("Mohammadi")
//                    .name("Soroush")
//                    .nationalCode("1234567891")
//                    .gender(GenderEnum.Male)
//                    .build(),Customer.builder().build());
//
//            customerService.saveLegal(LegalCustomer.builder()
//                    .openingDate("1380/01/01")
//                    .title("Dotin")
//                    .nationalCode("1234567891")
//                    .build(),Customer.builder().build());
//        }
//    }

//    @Test
//    void seedDepositType(){
//        depositService.saveDepositType(DepositType.builder()
//                .title("سپرده کوتاه مدت")
//                .isCurrent(false)
//                .build());
//        depositService.saveDepositType(DepositType.builder()
//                .title("قرض الحسنه جاری")
//                .isCurrent(true)
//                .build());
//    }

    @Test
    void testCountCustomer(){
         assertEquals(customerService.countRealCustomers(),1);
         assertEquals(customerService.countLegalCustomers(),1);
    }

}
