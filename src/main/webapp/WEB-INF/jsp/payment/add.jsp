<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ page import="ir.co.ppaz.corebanking.enums.PaymentTypeEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../template/header.jsp" %>

<div class="col-lg-12">

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                انجام تراکنش
            </h5>

            <!-- General Form Elements -->
            <f:form modelAttribute="payment" method="post" action="/payment/add">
                <div class="row mb-3">
                    <f:label path="date" class="col-sm-2 col-form-label">تاریخ</f:label>
                    <div class="col-sm-10">
                        <f:input path="date" type="text" class="form-control"/>
                    </div>
                </div>
                <div class="row mb-3">
                    <f:label path="time" class="col-sm-2 col-form-label">ساعت</f:label>
                    <div class="col-sm-10">
                        <f:input path="time" type="text" class="form-control"/>
                    </div>
                </div>
                <div class="row mb-3">
                    <f:label path="type" class="col-sm-2 col-form-label">نوع</f:label>
                    <div class="col-sm-10">
                        <f:select path="type" class="form-select">
                            <f:option value="${PaymentTypeEnum.Debtor}" >واریز</f:option>
                            <f:option value="${PaymentTypeEnum.Creditor}" >برداشت</f:option>
                        </f:select>
                    </div>
                </div>
                <div class="row mb-3">
                    <f:label path="amount" class="col-sm-2 col-form-label">مبلغ</f:label>
                    <div class="col-sm-10">
                        <f:input path="amount" type="text" class="form-control"/>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">ایجاد سپرده</button>
                    </div>
                </div>

                <f:hidden path="depositId"/>
            </f:form><!-- End General Form Elements -->

        </div>
    </div>

</div>

<%@include file="../template/footer.jsp" %>