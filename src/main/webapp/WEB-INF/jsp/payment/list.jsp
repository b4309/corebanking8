<%--
  Created by IntelliJ IDEA.
  User: mmohammadi
  Date: 2/7/22
  Time: 7:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="ir.co.ppaz.corebanking.enums.PaymentTypeEnum" %>
<%@include file="../template/header.jsp" %>


<div class="dataTable-wrapper dataTable-loading no-footer sortable searchable fixed-columns">
    <div class="dataTable-top">
        <div class="dataTable-info">
            <a href="/payment/add/${depositId}">+ Add New Payment</a>
        </div>
        <div class="dataTable-search">
            <input class="dataTable-input" placeholder="Search..." type="text">
        </div>
    </div>
    <div class="dataTable-container">
        <table class="table datatable dataTable-table">
            <thead>
            <tr>
                <th scope="col" data-sortable="" style="width: 5.63661%;"><a href="#" class="dataTable-sorter">#</a></th>
                <th scope="col" data-sortable="" style="width: 28.0504%;"><a href="#" class="dataTable-sorter">Type</a></th>
                <th scope="col" data-sortable="" style="width: 19.3634%;"><a href="#" class="dataTable-sorter">Date</a></th>
                <th scope="col" data-sortable="" style="width: 19.3634%;"><a href="#" class="dataTable-sorter">Prev Amount</a></th>
                <th scope="col" data-sortable="" style="width: 19.3634%;"><a href="#" class="dataTable-sorter">Amount</a></th>
                <th scope="col" data-sortable="" style="width: 19.3634%;"><a href="#" class="dataTable-sorter">New Amount</a></th>
                <th scope="col" data-sortable="" style="width: 19.3634%;"><a href="#" class="dataTable-sorter">...</a></th>
            </tr>
            </thead>
            <tbody>

                <c:choose>
                    <c:when test="${payments.size() eq 0}">
                        <tr>
                            <td colspan="5">
                                موردی برای نمایش وجود ندارد
                            </td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach items="${payments}" var="payment">
                            <tr>
                                <td>${payment.paymentId}</td>
                                <td>
                                    <c:choose>
                                        <c:when test="${payment.type eq PaymentTypeEnum.Debtor}">
                                            واریز
                                        </c:when>
                                        <c:otherwise>
                                            برداشت
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>${payment.date} ${payment.time}</td>
                                <td>${payment.prevAmount}</td>
                                <td>${payment.amount}</td>
                                <td>${payment.newAmount}</td>
                                <td></td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>

</div>

<%@include file="../template/footer.jsp" %>