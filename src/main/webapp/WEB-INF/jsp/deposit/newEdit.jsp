<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../template/header.jsp" %>

<div class="col-lg-12">

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <c:choose>
                    <c:when test="${deposit.number eq null}">
                        افتتاح سپرده جدید
                    </c:when>
                    <c:otherwise>
                        ویرایش سپرده شماره ${deposit.number} - ${deposit.title}
                    </c:otherwise>
                </c:choose>
            </h5>

            <!-- General Form Elements -->
            <f:form modelAttribute="deposit" method="post" action="/deposit/add">
                <div class="row mb-3">
                    <f:label path="openingDate" class="col-sm-2 col-form-label">تاریخ افتتاح</f:label>
                    <div class="col-sm-10">
                        <f:input path="openingDate" type="text" class="form-control"/>
                    </div>
                </div>
                <div class="row mb-3">
                    <f:label path="expDate" class="col-sm-2 col-form-label">تاریخ انقضا</f:label>
                    <div class="col-sm-10">
                        <f:input path="expDate" type="text" class="form-control"/>
                    </div>
                </div>
                <c:choose>
                    <c:when test="${deposit.number eq null}">

                    </c:when>
                    <c:otherwise>
                        <div class="row mb-3">
                            <div class="form-check">
                                <f:checkbox path="isOpen" class="form-check-input" disabled="true"/>
                                <f:label path="isOpen" class="form-check-label">
                                    باز بودن
                                </f:label>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>

                <div class="row mb-3">
                    <f:label path="typeId" class="col-sm-2 col-form-label">نوع سپرده</f:label>
                    <div class="col-sm-10">
                        <f:select path="typeId" class="form-select">
                            <c:forEach items="${depositTypeList}" var="depositType" >
                                <f:option value="${depositType.id}" >${depositType.title}</f:option>
                            </c:forEach>
<%--                            <f:options items="${depositTypeList}" itemLabel="title" itemValue="id"  />--%>
                        </f:select>
                        <a href="deposit/addDepositType">ایجاد نوع سپرده</a>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">ایجاد سپرده</button>
                    </div>
                </div>

                <f:hidden path="id"/>
            </f:form><!-- End General Form Elements -->

        </div>
    </div>

</div>

<%@include file="../template/footer.jsp" %>