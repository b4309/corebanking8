<%--
  Created by IntelliJ IDEA.
  User: mmohammadi
  Date: 2/7/22
  Time: 7:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../template/header.jsp" %>

<div class="dataTable-wrapper dataTable-loading no-footer sortable searchable fixed-columns">
    <div class="dataTable-top">
        <div class="dataTable-info">
            <a href="/deposit/add">+ Add New Deposit</a>
        </div>
        <div class="dataTable-search">
            <input class="dataTable-input" placeholder="Search..." type="text">
        </div>
    </div>
    <div class="dataTable-container">
        <table class="table datatable dataTable-table">
            <thead>
            <tr>
                <th scope="col" data-sortable="" style="width: 5.63661%;"><a href="#" class="dataTable-sorter">#</a></th>
                <th scope="col" data-sortable="" style="width: 28.0504%;"><a href="#" class="dataTable-sorter">Title</a></th>
                <th scope="col" data-sortable="" style="width: 19.3634%;"><a href="#" class="dataTable-sorter">Opening Date</a></th>
                <th scope="col" data-sortable="" style="width: 19.3634%;"><a href="#" class="dataTable-sorter">Type</a></th>
                <th scope="col" data-sortable="" style="width: 19.3634%;"><a href="#" class="dataTable-sorter">Status</a></th>
                <th scope="col" data-sortable="" style="width: 19.3634%;"><a href="#" class="dataTable-sorter">...</a></th>
            </tr>
            </thead>
            <tbody>

                <c:choose>
                    <c:when test="${deposits.size() eq 0}">
                        <tr>
                            <td colspan="5">
                                موردی برای نمایش وجود ندارد
                            </td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach items="${deposits}" var="deposit">
                            <tr>
                                <td>${deposit.number}</td>
                                <td>${deposit.title}</td>
                                <td>${deposit.openingDate}</td>
                                <td>${deposit.type.title}</td>
                                <td>
                                    <c:choose>
                                        <c:when test="${deposit.isOpen}">
                                            باز
                                        </c:when>
                                        <c:otherwise>
                                            مسدود
                                        </c:otherwise>
                                    </c:choose>
                                    /
                                    <c:choose>
                                        <c:when test="${deposit.type.isCurrent}">
                                            جاری
                                        </c:when>
                                        <c:otherwise>
                                            سپرده
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    <a href="/deposit/edit/${deposit.id}">ویرایش</a>
                                    /<a href="/deposit/delete/${deposit.id}">حذف</a>
                                    /<a href="/customer/showDepositInfo/${deposit.id}">اطلاعات مشتریان</a>
                                    <c:choose>
                                        <c:when test="${deposit.isOpen}">
                                            /<a href="/deposit/close/${deposit.id}">بستن سپرده</a>
                                            /<a href="/payment/${deposit.id}">عملیات مالی</a>
                                        </c:when>
                                        <c:otherwise>
                                            /<a href="/deposit/open/${deposit.id}">باز کردن سپرده</a>
                                        </c:otherwise>
                                    </c:choose>

                                </td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>

</div>

<%@include file="../template/footer.jsp" %>