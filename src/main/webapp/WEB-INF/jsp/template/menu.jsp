<%--
  Created by IntelliJ IDEA.
  User: mmohammadi
  Date: 2/7/22
  Time: 7:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- ======= Sidebar ======= -->
<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

        <li class="nav-item">
            <a class="nav-link " href="/">
                <i class="bi bi-grid"></i>
                <span>Dashboard</span>
            </a>
        </li><!-- End Dashboard Nav -->

        <li class="nav-item">
            <a class="nav-link " href="/deposit/">
                <i class="bi bi-grid"></i>
                <span>Deposit Manager</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link " href="/customer/">
                <i class="bi bi-grid"></i>
                <span>Customer Manager</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link " href="/employee/logout">
                <i class="bi bi-grid"></i>
                <span>Logout</span>
            </a>
        </li>
    </ul>

</aside><!-- End Sidebar-->

