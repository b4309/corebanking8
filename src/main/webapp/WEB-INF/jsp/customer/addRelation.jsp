<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../template/header.jsp" %>

<div class="col-lg-12">

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                اضافه کردن ارتباط مشتری با حساب
            </h5>

            <!-- General Form Elements -->
            <f:form modelAttribute="customerRelation" method="post" action="/customer/addRelation">
                <div class="row mb-3">
                    <f:label path="customerId" class="col-sm-2 col-form-label">مشتری</f:label>
                    <div class="col-sm-10">
                        <f:select path="customerId" class="form-select">
                            <c:forEach items="${customerList}" var="customer" >
                                <f:option value="${customer.customerId}" >${customer.title}</f:option>
                            </c:forEach>
                        </f:select>
                    </div>
                </div>
                <div class="row mb-3">
                    <f:label path="portion" class="col-sm-2 col-form-label">درصد</f:label>
                    <div class="col-sm-10">
                        <f:input path="portion" type="text" class="form-control"/>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">اضافه کردن رابطه</button>
                    </div>
                </div>

                <f:hidden path="depositId" />
            </f:form><!-- End General Form Elements -->

        </div>
    </div>

</div>

<%@include file="../template/footer.jsp" %>