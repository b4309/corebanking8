<%--
  Created by IntelliJ IDEA.
  User: mmohammadi
  Date: 2/7/22
  Time: 7:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="ir.co.ppaz.corebanking.enums.GenderEnum" %>
<%@include file="../template/header.jsp" %>

<div class="dataTable-wrapper dataTable-loading no-footer sortable searchable fixed-columns">
    <div class="dataTable-top">
        <div class="dataTable-info">
            <a href="/customer/addReal">+ Add New Real Customer</a>
            <a href="/customer/addLegal">+ Add New Legal Customer</a>
        </div>
        <div class="dataTable-search">
            <input class="dataTable-input" placeholder="Search..." type="text">
        </div>
    </div>
    <div class="dataTable-container">
        <table class="table datatable dataTable-table">
            <thead>
            <tr>
                <th scope="col" data-sortable="" style="width: 5.63661%;"><a href="#" class="dataTable-sorter">#</a></th>
                <th scope="col" data-sortable="" style="width: 28.0504%;"><a href="#" class="dataTable-sorter">Type</a></th>
                <th scope="col" data-sortable="" style="width: 19.3634%;"><a href="#" class="dataTable-sorter">Title</a></th>
                <th scope="col" data-sortable="" style="width: 19.3634%;"><a href="#" class="dataTable-sorter">ID</a></th>
                <th scope="col" data-sortable="" style="width: 19.3634%;"><a href="#" class="dataTable-sorter">Status</a></th>
                <th scope="col" data-sortable="" style="width: 19.3634%;"><a href="#" class="dataTable-sorter">...</a></th>
            </tr>
            </thead>
            <tbody>

                <c:choose>
                    <c:when test="${customers.size() eq 0}">
                        <tr>
                            <td colspan="5">
                                موردی برای نمایش وجود ندارد
                            </td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach items="${customers}" var="customer">
                            <tr>
                                <td>${customer.id}</td>
                                <td>
                                    <c:choose>
                                        <c:when test="${customer.isReal}">
                                            حقیقی
                                        </c:when>
                                        <c:otherwise>
                                            حقوقی
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    <c:choose>
                                        <c:when test="${customer.isReal}">
                                            <c:choose>
                                                <c:when test="${customer.real.gender eq GenderEnum.Male}">
                                                    آقای
                                                </c:when>
                                                <c:otherwise>
                                                    خانم
                                                </c:otherwise>
                                            </c:choose>
                                            ${customer.real.name} ${customer.real.family}
                                        </c:when>
                                        <c:otherwise>
                                            ${customer.legal.title}
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    <c:choose>
                                        <c:when test="${customer.isReal}">
                                            ${customer.real.nationalCode}
                                        </c:when>
                                        <c:otherwise>
                                            ${customer.legal.nationalCode}
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    ...
                                </td>
                                <td>
                                    <a href="/customer/edit/${customer.id}">ویرایش</a>
                                    /<a href="/customer/delete/${customer.id}">حذف</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>

</div>

<%@include file="../template/footer.jsp" %>