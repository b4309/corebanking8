package ir.co.ppaz.corebanking.endpoint;

import ir.co.ppaz.corebanking.core.CoreBankingException;
import ir.co.ppaz.corebanking.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/api/deposit")
public class DepositEndPoint {

    @Autowired
    DepositService depositService;

    @GetMapping("/list")
    public ResponseEntity<Object> getList(){
        try {
//            throw new CoreBankingException(ErrorCodes.THE_FIELD_IS_EMPTY,"دوست دارم خطا بدم");
            return ResponseEntity.status(HttpStatus.OK).body(depositService.getAllDeposit());
        } catch (CoreBankingException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new HashMap<>(){{put(e.getCode(),e.getMessage());}});
        }
    }

}
