package ir.co.ppaz.corebanking.codes;

public class ErrorCodes {

    public static final String THE_FIELD_IS_EMPTY = "001";
    public static final String PASSWORD_MUST_6 = "002";
    public static final String USER_NOT_FOUND = "003";
    public static final String ITEM_IS_EMPTY = "004";
    public static final String DEPOSIT_TYPE_NOT_FOUND = "005";
    public static final String DEPOSIT_NOT_FOUND = "006";
    public static final String CUSTOMER_NOT_FOUND = "007";
    public static final String PORTION_OVER = "008";
    public static final String AMOUNT_OVER = "009";
    public static final String PAYMENT_NOT_FOUND = "010";

}
