package ir.co.ppaz.corebanking.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "DepositType")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DepositType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String title;
    private Boolean isCurrent;

    @OneToMany(mappedBy = "type", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Deposit> deposits;
}
