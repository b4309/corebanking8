package ir.co.ppaz.corebanking.model;

import ir.co.ppaz.corebanking.enums.GenderEnum;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "RealCustomer")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RealCustomer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String family;
    private GenderEnum gender;
    private String birthDate;
    private String deathDate;
    private String nationalCode;

    @OneToMany(mappedBy = "real",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Customer> customer;

}
