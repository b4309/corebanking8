package ir.co.ppaz.corebanking.model;

import ir.co.ppaz.corebanking.enums.BlackListTypeEnum;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "BlackListCustomer")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BlackListCustomer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String startDate;
    private String endDate;
    private Boolean isActive;
    private BlackListTypeEnum type;

    @ManyToOne
    private Customer customer;

}
