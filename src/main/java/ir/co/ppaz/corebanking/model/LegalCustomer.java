package ir.co.ppaz.corebanking.model;

import ir.co.ppaz.corebanking.enums.GenderEnum;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "LegalCustomer")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LegalCustomer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String title;
    private String openingDate;
    private String closeDate;
    private String nationalCode;

    @OneToMany(mappedBy = "legal",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Customer> customer;
}
