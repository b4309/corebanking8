package ir.co.ppaz.corebanking.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Deposit")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Deposit implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String number;
    private String title;
    private String openingDate;
    private String expDate;
    private Boolean isOpen;

    @ManyToOne
    private DepositType type;
    @OneToMany(mappedBy = "deposit", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Payment> payments;
    @OneToMany(mappedBy = "deposit", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<CustomerRelation> relations;

}
