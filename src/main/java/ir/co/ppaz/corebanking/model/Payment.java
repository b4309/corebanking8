package ir.co.ppaz.corebanking.model;

import ir.co.ppaz.corebanking.enums.PaymentTypeEnum;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Payment")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Payment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String date;
    private String time;
    private Long prevAmount;
    private Long amount;
    private Long newAmount;
    private PaymentTypeEnum type;

    @ManyToOne
    private Deposit deposit;

}
