package ir.co.ppaz.corebanking.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CustomerRelation")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerRelation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer portion;

    @ManyToOne
    private Deposit deposit;
    @ManyToOne
    private Customer customer;
}
