package ir.co.ppaz.corebanking.service;

import ir.co.ppaz.corebanking.core.CoreBankingException;
import ir.co.ppaz.corebanking.dto.DepositNewEditDto;
import ir.co.ppaz.corebanking.dto.DepositTypeItemFoeCBDto;
import ir.co.ppaz.corebanking.model.Deposit;
import ir.co.ppaz.corebanking.model.DepositType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DepositService {

    List<Deposit> getAllDeposit() throws CoreBankingException;

    void saveDeposit(DepositNewEditDto dto) throws CoreBankingException;

    List<DepositTypeItemFoeCBDto> getDepositTypeItemsFoeCbDto() throws CoreBankingException;

    void saveDepositType(DepositType depositType);

    Deposit getDepositById(Integer depositId) throws CoreBankingException;
}
