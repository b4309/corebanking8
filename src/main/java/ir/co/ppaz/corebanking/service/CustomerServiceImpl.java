package ir.co.ppaz.corebanking.service;

import ir.co.ppaz.corebanking.codes.ErrorCodes;
import ir.co.ppaz.corebanking.core.CoreBankingException;
import ir.co.ppaz.corebanking.dto.DepositCustomer;
import ir.co.ppaz.corebanking.enums.GenderEnum;
import ir.co.ppaz.corebanking.model.*;
import ir.co.ppaz.corebanking.repository.CustomerRelationRepository;
import ir.co.ppaz.corebanking.repository.CustomerRepository;
import ir.co.ppaz.corebanking.repository.LegalCustomerRepository;
import ir.co.ppaz.corebanking.repository.RealCustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    RealCustomerRepository realCustomerRepository;
    @Autowired
    LegalCustomerRepository legalCustomerRepository;
    @Autowired
    CustomerRelationRepository customerRelationRepository;

    @Autowired
    DepositService depositService;

    @Override
    public List<Customer> getAllCustomers() {
        return (ArrayList<Customer>)customerRepository.findAll();
    }

    @Override
    public void saveReal(RealCustomer realCustomer,Customer customer) {
        realCustomerRepository.save(realCustomer);
        customer.setIsReal(true);
        customer.setReal(realCustomer);
        customerRepository.save(customer);
    }

    @Override
    public void saveLegal(LegalCustomer legalCustomer,Customer customer) {
        legalCustomerRepository.save(legalCustomer);
        customer.setIsReal(false);
        customer.setLegal(legalCustomer);
        customerRepository.save(customer);
    }

    @Override
    public Long countRealCustomers() {
//        customerRepository.count();
        return ((ArrayList<Customer>) customerRepository.findAll()).stream().filter(Customer::getIsReal).count();
    }

    @Override
    public Long countLegalCustomers() {
        return customerRepository.countLegalCustomer();
    }

    @Override
    public List<DepositCustomer> getDepositCustomers(Deposit deposit) {
        List<DepositCustomer> depositCustomers = new ArrayList<>();
        List<CustomerRelation> relations = customerRelationRepository.findCustomerRelationByDeposit(deposit);
        relations.forEach(customerRelation -> {
            Boolean isReal = customerRelation.getCustomer().getIsReal();
            depositCustomers.add(DepositCustomer.builder()
                    .customerId(customerRelation.getCustomer().getId())
                    .relationId(customerRelation.getId())
                    .depositId(customerRelation.getDeposit().getId())
                    .isReal(isReal)
                    .nationalId(isReal ? customerRelation.getCustomer().getReal().getNationalCode() : customerRelation.getCustomer().getLegal().getNationalCode())
                    .portion(customerRelation.getPortion())
                    .title(isReal ?
                            (customerRelation.getCustomer().getReal().getGender() == GenderEnum.Male ? "آقای" : "خانم") + " " + customerRelation.getCustomer().getReal().getName() + " " + customerRelation.getCustomer().getReal().getFamily() :
                            "شرکت " + customerRelation.getCustomer().getLegal().getTitle())
                    .build());
        });
        return depositCustomers;
    }

    @Override
    public List<DepositCustomer> getAllCustomersForRelation() {
        List<DepositCustomer> depositCustomers = new ArrayList<>();
        List<Customer> allCustomers = getAllCustomers();
        allCustomers.forEach(customer -> {
            Boolean isReal = customer.getIsReal();
            depositCustomers.add(DepositCustomer.builder()
                    .customerId(customer.getId())
                    .depositId(0)
                    .relationId(0)
                    .isReal(isReal)
                    .nationalId(isReal ? customer.getReal().getNationalCode() : customer.getLegal().getNationalCode())
                    .portion(0)
                    .title(isReal ?
                            (customer.getReal().getGender() == GenderEnum.Male ? "آقای" : "خانم") + " " + customer.getReal().getName() + " " + customer.getReal().getFamily() :
                            "شرکت " + customer.getLegal().getTitle())
                    .build());
        });
        return depositCustomers;
    }

    @Override
    public void addRelation(DepositCustomer depositCustomer) throws CoreBankingException {
        if (depositCustomer.getPortion() > 100 || depositCustomer.getPortion() < 0){
            throw new CoreBankingException(ErrorCodes.PORTION_OVER,"محدوده مجاز برای درصد بین 0 تا 100 می باشد.");
        }
        Customer customer = customerRepository.findById(depositCustomer.getCustomerId()).orElseThrow(() -> new CoreBankingException(ErrorCodes.CUSTOMER_NOT_FOUND, "مشتری مورد نظر یافت نشد"));
        Deposit deposit = depositService.getDepositById(depositCustomer.getDepositId());

        //1->
        Long sumPortionOfDeposit = customerRelationRepository.sumPortionOfDeposit(deposit.getNumber());
        if(sumPortionOfDeposit + depositCustomer.getPortion() > 100) {
            throw new CoreBankingException(ErrorCodes.PORTION_OVER,"مجموع مقادیر درصد سهم نباید بیشتر از 100 باشد.");
        }

        //2->
//        AtomicReference<Long> sumPortionOfDeposit2 = new AtomicReference<>(0L);
////        customerRelationRepository.findCustomerRelationByDeposit(deposit).stream().forEach(customerRelation -> sumPortionOfDeposit2.updateAndGet(v -> v + customerRelation.getPortion()));
//        long sum = customerRelationRepository.findCustomerRelationByDeposit(deposit).stream().mapToLong(CustomerRelation::getPortion).sum();
//        if(sum + depositCustomer.getPortion() > 100) {
//            throw new CoreBankingException(ErrorCodes.PORTION_OVER,"مجموع مقادیر درصد سهم نباید بیشتر از 100 باشد.");
//        }

        CustomerRelation customerRelation = CustomerRelation.builder()
                .customer(customer)
                .deposit(deposit)
                .portion(depositCustomer.getPortion())
                .build();

        customerRelationRepository.save(customerRelation);
    }
}
