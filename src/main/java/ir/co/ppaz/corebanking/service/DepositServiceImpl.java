package ir.co.ppaz.corebanking.service;

import ir.co.ppaz.corebanking.codes.ErrorCodes;
import ir.co.ppaz.corebanking.core.CoreBankingException;
import ir.co.ppaz.corebanking.dto.DepositNewEditDto;
import ir.co.ppaz.corebanking.dto.DepositTypeItemFoeCBDto;
import ir.co.ppaz.corebanking.model.Deposit;
import ir.co.ppaz.corebanking.model.DepositType;
import ir.co.ppaz.corebanking.repository.DepositRepository;
import ir.co.ppaz.corebanking.repository.DepositTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class DepositServiceImpl implements DepositService {

    @Autowired
    DepositRepository depositRepository;
    @Autowired
    DepositTypeRepository depositTypeRepository;

    @Override
    public List<Deposit> getAllDeposit() throws CoreBankingException {
        List<Deposit> deposits = (List<Deposit>) depositRepository.findAll();
        if (Objects.isNull(deposits) || deposits.isEmpty()) {
            throw new CoreBankingException(ErrorCodes.ITEM_IS_EMPTY, "سپرده برای مشاهده وجود ندارد");
        }
        return deposits;
    }

    @Override
    public void saveDeposit(DepositNewEditDto dto) throws CoreBankingException {
        if (Objects.isNull(dto.getTypeId())) {
            throw new CoreBankingException(ErrorCodes.THE_FIELD_IS_EMPTY, "انتخاب نوع سپرده الزامی است");
        }
        DepositType depositType = depositTypeRepository.findById(dto.getTypeId()).orElseThrow(() -> new CoreBankingException(ErrorCodes.DEPOSIT_TYPE_NOT_FOUND, "نوع سپرده یافت نشد"));
        if (Objects.isNull(dto.getId())) {
            //save
            dto.setIsOpen(true);
            if (Objects.isNull(dto.getOpeningDate())) {
                throw new CoreBankingException(ErrorCodes.THE_FIELD_IS_EMPTY, "وارد کردن تاریخ افتتاح الزامی است");
            }
            dto.setNumber(generateDepositNumber(dto));
            dto.setTitle(generateDepositTitle(dto, depositType));
            Deposit deposit = convertDepositDtoToDeposit(dto, depositType);
            depositRepository.save(deposit);
        } else {
            //edit
        }
    }

    private Deposit convertDepositDtoToDeposit(DepositNewEditDto dto, DepositType depositType) {
        return Deposit.builder()
                .id(dto.getId())
                .number(dto.getNumber())
                .title(dto.getTitle())
                .openingDate(dto.getOpeningDate())
                .expDate((Objects.isNull(dto.getExpDate()) || dto.getExpDate().isEmpty() ? null : dto.getExpDate()))
                .isOpen(dto.getIsOpen())
                .type(depositType)
                .build();
    }

    private String generateDepositTitle(DepositNewEditDto dto, DepositType depositType) {
        return depositType.getTitle() + " " + dto.getNumber() + " /" + (depositType.getIsCurrent() ? "جاری" : "سپرده");
    }

    private String generateDepositNumber(DepositNewEditDto dto) {
        //depositTypeId + . + YearOpening + . + uniqCode(6 digit)
        String lastNumber = depositRepository.getLastNumber();
        if (Objects.isNull(lastNumber) || lastNumber.isEmpty()) {
            lastNumber = "159159";
        } else {
            lastNumber = String.valueOf(Integer.parseInt(lastNumber.split("\\.")[2]) + 1);
        }
        return dto.getTypeId() + "." + dto.getOpeningDate().split("/")[0] + "." + lastNumber;
    }

    @Override
    public List<DepositTypeItemFoeCBDto> getDepositTypeItemsFoeCbDto() throws CoreBankingException {
//        List<DepositTypeItemFoeCBDto> dtos = new ArrayList<>();
        Iterable<DepositType> depositTypes = depositTypeRepository.findAll();
        // For Loop
//        for (DepositType depositType : depositTypes){
        // 1 POJO
//            DepositTypeItemFoeCBDto dto = new DepositTypeItemFoeCBDto();
//            dto.setId(depositType.getId());
//            dto.setTitle(depositType.getTitle());
//            dtos.add(dto);

        // 2 All Constructor
//            dtos.add(new DepositTypeItemFoeCBDto(depositType.getId(),depositType.getTitle()));

        // 3 Builder
//            dtos.add(DepositTypeItemFoeCBDto.builder().id(depositType.getId()).title(depositType.getTitle()).build());
//        }

        //Stream
//        depositTypes.forEach(depositType -> {
        // 4 POJO
//            DepositTypeItemFoeCBDto dto = new DepositTypeItemFoeCBDto();
//            dto.setId(depositType.getId());
//            dto.setTitle(depositType.getTitle());
//            dtos.add(dto);

        // 5 All Constructor
//            dtos.add(new DepositTypeItemFoeCBDto(depositType.getId(),depositType.getTitle()));

        // 6 Builder
//            dtos.add(DepositTypeItemFoeCBDto.builder().id(depositType.getId()).title(depositType.getTitle()).build());
//        });

        List<DepositType> depositTypesList = (ArrayList) depositTypes;
        // 7
//        depositTypesList.stream().map(depositType -> DepositTypeItemFoeCBDto.builder().id(depositType.getId()).title(depositType.getTitle()).build()).collect(Collectors.toList());
        // 8
        return depositTypesList.stream().map(DepositTypeItemFoeCBDto::new).collect(Collectors.toList());

        // Find only notCurrent Deposits
//        depositTypesList.stream().filter(depositType -> depositType.getIsCurrent()==true).collect(Collectors.toList());
//        depositTypesList.stream().filter(DepositType::getIsCurrent).collect(Collectors.toList());

        //
//        depositTypesList.stream().forEach(depositType -> {
        // 4 POJO
//            DepositTypeItemFoeCBDto dto = new DepositTypeItemFoeCBDto();
//            dto.setId(depositType.getId());
//            dto.setTitle(depositType.getTitle());
//            dtos.add(dto);

        // 5 All Constructor
//            dtos.add(new DepositTypeItemFoeCBDto(depositType.getId(),depositType.getTitle()));

        // 6 Builder
//            dtos.add(DepositTypeItemFoeCBDto.builder().id(depositType.getId()).title(depositType.getTitle()).build());
//        });

        //get first 5 item
//        depositTypesList.stream().limit(5);

//        depositTypesList.stream().findFirst().orElse(null);
//        depositTypesList.stream().findFirst().orElseThrow(()-> new CoreBankingException("",""));

//        depositTypesList.stream().sorted(Comparator.comparingInt(DepositType::getId).reversed());

//        depositTypesList.stream().
    }

    @Override
    public void saveDepositType(DepositType depositType) {
        depositTypeRepository.save(depositType);
    }

    @Override
    public Deposit getDepositById(Integer depositId) throws CoreBankingException {
        return depositRepository.findById(depositId).orElseThrow(()->new CoreBankingException(ErrorCodes.DEPOSIT_NOT_FOUND,"سپرده ای با این شناسه یافت نشد."));
    }
}
