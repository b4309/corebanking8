package ir.co.ppaz.corebanking.service;

import ir.co.ppaz.corebanking.codes.ErrorCodes;
import ir.co.ppaz.corebanking.core.CoreBankingException;
import ir.co.ppaz.corebanking.dto.PaymentDto;
import ir.co.ppaz.corebanking.enums.PaymentTypeEnum;
import ir.co.ppaz.corebanking.model.Deposit;
import ir.co.ppaz.corebanking.model.Payment;
import ir.co.ppaz.corebanking.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    DepositService depositService;

    @Override
    public List<PaymentDto> getDepositPayments(Integer depositId) throws CoreBankingException {
        Deposit deposit = depositService.getDepositById(depositId);
        List<Payment> paymentsByDeposit = paymentRepository.getPaymentsByDepositOrderByDate(deposit);
        List<PaymentDto> paymentDtos = new ArrayList<>();
        paymentsByDeposit.forEach(payment -> paymentDtos.add(PaymentDto.builder()
                .paymentId(payment.getId())
                .depositId(depositId)
                .amount(payment.getAmount())
                .date(payment.getDate())
                .type(payment.getType())
                .newAmount(payment.getNewAmount())
                .prevAmount(payment.getPrevAmount())
                .time(payment.getTime())
                .build()));

        return paymentDtos;
    }

    @Override
    public void add(PaymentDto paymentDto) throws CoreBankingException {
        validatePayment(paymentDto);
        validateAmount(paymentDto);
        Payment payment = convertDtoToPayment(paymentDto);
        paymentRepository.save(payment);
    }

    private Payment convertDtoToPayment(PaymentDto paymentDto) throws CoreBankingException {
        Payment lastPaymentByDeposit = paymentRepository.getLastPaymentByDeposit(paymentDto.getDepositId());

        Long prevAmount = 0L;
        if(Objects.nonNull(lastPaymentByDeposit)){
            prevAmount = lastPaymentByDeposit.getNewAmount();
        }

        Long newAmount = 0L;
        if(paymentDto.getType() == PaymentTypeEnum.Debtor){
            newAmount = prevAmount + paymentDto.getAmount();
        }else{
            newAmount = prevAmount - paymentDto.getAmount();
        }

        return Payment.builder()
                .amount(paymentDto.getAmount())
                .date(paymentDto.getDate())
                .deposit(depositService.getDepositById(paymentDto.getDepositId()))
                .prevAmount(prevAmount)
                .amount(paymentDto.getAmount())
                .newAmount(newAmount)
                .type(paymentDto.getType())
                .time(paymentDto.getTime())
                .build();
    }

    private void validateAmount(PaymentDto paymentDto)  throws CoreBankingException {
        if(paymentDto.getType() == PaymentTypeEnum.Creditor) {
            Payment lastPaymentByDeposit = paymentRepository.getLastPaymentByDeposit(paymentDto.getDepositId());
            if(Objects.isNull(lastPaymentByDeposit)){
                throw new CoreBankingException(ErrorCodes.PAYMENT_NOT_FOUND,"هیچ تراکنشی روی این حساب اتفاق نیافتاده است. پس نمیتوانید از این حساب برداشت کنید.");
            }
            if(lastPaymentByDeposit.getNewAmount() < paymentDto.getAmount()){
                throw new CoreBankingException(ErrorCodes.AMOUNT_OVER, "مبلغ درخواستی از موجودی حساب بیشتر است.");
            }
        }
    }

    private void validatePayment(PaymentDto paymentDto) throws CoreBankingException {
        if(Objects.isNull(paymentDto.getDate()) || Objects.isNull(paymentDto.getTime())){
            throw new CoreBankingException(ErrorCodes.ITEM_IS_EMPTY,"زمان تراکنش نمیتواند خالی باشد");
        }
        if(paymentDto.getAmount() <= 0) {
            throw new CoreBankingException(ErrorCodes.AMOUNT_OVER, "مبلغ باید بیشتر از صفر باشد");
        }
    }
}
