package ir.co.ppaz.corebanking.service;

import ir.co.ppaz.corebanking.core.CoreBankingException;
import ir.co.ppaz.corebanking.dto.PaymentDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PaymentService {

    List<PaymentDto> getDepositPayments(Integer depositId) throws CoreBankingException;

    void add(PaymentDto paymentDto) throws CoreBankingException;
}
