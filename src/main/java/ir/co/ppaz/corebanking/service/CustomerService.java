package ir.co.ppaz.corebanking.service;

import ir.co.ppaz.corebanking.core.CoreBankingException;
import ir.co.ppaz.corebanking.dto.DepositCustomer;
import ir.co.ppaz.corebanking.model.Customer;
import ir.co.ppaz.corebanking.model.Deposit;
import ir.co.ppaz.corebanking.model.LegalCustomer;
import ir.co.ppaz.corebanking.model.RealCustomer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CustomerService {

    List<Customer> getAllCustomers();

    void saveReal(RealCustomer realCustomer,Customer customer);

    void saveLegal(LegalCustomer legalCustomer,Customer customer);

    Long countRealCustomers();

    Long countLegalCustomers();

    List<DepositCustomer> getDepositCustomers(Deposit deposit);

    List<DepositCustomer> getAllCustomersForRelation();

    void addRelation(DepositCustomer depositCustomer) throws CoreBankingException;
}
