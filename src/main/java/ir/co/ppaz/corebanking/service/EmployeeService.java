package ir.co.ppaz.corebanking.service;

import ir.co.ppaz.corebanking.core.CoreBankingException;
import ir.co.ppaz.corebanking.dto.UserDto;
import ir.co.ppaz.corebanking.model.Employee;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EmployeeService {

    Employee login(UserDto userDto) throws CoreBankingException;

    void save(Employee employee);

    List<Employee> findAllEmployee();
}
