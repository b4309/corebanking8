package ir.co.ppaz.corebanking.service;

import ir.co.ppaz.corebanking.codes.ErrorCodes;
import ir.co.ppaz.corebanking.core.CoreBankingException;
import ir.co.ppaz.corebanking.dto.UserDto;
import ir.co.ppaz.corebanking.model.Employee;
import ir.co.ppaz.corebanking.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Override
    public Employee login(UserDto userDto) throws CoreBankingException {
        validateLogin(userDto);
        Employee employee = employeeRepository.login(userDto.getUsername(), userDto.getPassword());
        if (Objects.isNull(employee)) {
            throw new CoreBankingException(ErrorCodes.USER_NOT_FOUND, "کاربری با این مشخصات یافت نشد");
        }
        return employee;
    }

    @Override
    public void save(Employee employee) {
        employeeRepository.save(employee);
    }

    @Override
    public List<Employee> findAllEmployee() {
        return (ArrayList<Employee>)employeeRepository.findAll();
    }

    private void validateLogin(UserDto userDto) throws CoreBankingException {
        if (Objects.isNull(userDto.getUsername())
                || Objects.isNull(userDto.getPassword())
                || userDto.getUsername().isEmpty()
                || userDto.getPassword().isEmpty()
        ) {
            throw new CoreBankingException(ErrorCodes.THE_FIELD_IS_EMPTY, "نام کاربری یا رمز عبور خالی است");
        }

        if (userDto.getPassword().length() < 6) {
            throw new CoreBankingException(ErrorCodes.PASSWORD_MUST_6, "طول رمز عبور باید بیشتر از 6 کاراکتر باشد");
        }
    }
}
