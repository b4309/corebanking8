package ir.co.ppaz.corebanking.enums;

public enum PaymentTypeEnum {

    Debtor,
    Creditor;

}
