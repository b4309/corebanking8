package ir.co.ppaz.corebanking.controller;

import ir.co.ppaz.corebanking.codes.DefaultCodes;
import ir.co.ppaz.corebanking.core.CoreBankingException;
import ir.co.ppaz.corebanking.dto.DepositNewEditDto;
import ir.co.ppaz.corebanking.model.Deposit;
import ir.co.ppaz.corebanking.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@RequestMapping("/deposit")
public class DepositController {

    @Autowired
    DepositService depositService;

    @GetMapping("/")
    public ModelAndView list(HttpServletRequest servletRequest){
        if(Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))){
            return new ModelAndView("redirect:/");
        }
        List<Deposit> deposits = null;
        try {
            deposits = depositService.getAllDeposit();
        } catch (CoreBankingException e) {

        }
        List<Deposit> finalDeposits = Objects.isNull(deposits) ? new ArrayList<>() : deposits;
        return new ModelAndView("deposit/list", new HashMap<>() {{
            put("deposits", finalDeposits);
        }});
    }

    @GetMapping("/add")
    public ModelAndView add(HttpServletRequest servletRequest){
        if(Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))){
            return new ModelAndView("redirect:/");
        }
        return new ModelAndView("deposit/newEdit", new HashMap<String, Object>(){{
            put("deposit", new DepositNewEditDto());
            try {
                put("depositTypeList", depositService.getDepositTypeItemsFoeCbDto());
            } catch (CoreBankingException e) {

            }
        }});
    }

    @PostMapping("/add")
    public ModelAndView add(@ModelAttribute DepositNewEditDto dto, HttpServletRequest servletRequest){
        if(Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))){
            return new ModelAndView("redirect:/");
        }
        try {
            depositService.saveDeposit(dto);
        }catch (CoreBankingException e) {
            return new ModelAndView("errors/error", new HashMap() {{
                put("code", e.getCode());
                put("message", e.getMessage());
            }});
        }
        return new ModelAndView("redirect:/deposit/");
    }

    @GetMapping("/edit/{depositId}")
    public ModelAndView edit(@PathVariable Integer depositId, HttpServletRequest servletRequest){
        if(Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))){
            return new ModelAndView("redirect:/");
        }
        throw new IllegalArgumentException();
    }

    @GetMapping("/delete/{depositId}")
    public ModelAndView delete(@PathVariable Integer depositId, HttpServletRequest servletRequest){
        if(Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))){
            return new ModelAndView("redirect:/");
        }
        throw new IllegalArgumentException();
    }

    @GetMapping("/close/{depositId}")
    public ModelAndView close(@PathVariable Integer depositId, HttpServletRequest servletRequest){
        if(Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))){
            return new ModelAndView("redirect:/");
        }
        throw new IllegalArgumentException();
    }

    @GetMapping("/open/{depositId}")
    public ModelAndView open(@PathVariable Integer depositId, HttpServletRequest servletRequest){
        if(Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))){
            return new ModelAndView("redirect:/");
        }
        throw new IllegalArgumentException();
    }

    @GetMapping("/addDepositType")
    public ModelAndView addDepositType( HttpServletRequest servletRequest){
        if(Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))){
            return new ModelAndView("redirect:/");
        }
        throw new IllegalArgumentException();
    }


}
