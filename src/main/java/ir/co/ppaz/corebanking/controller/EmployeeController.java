package ir.co.ppaz.corebanking.controller;

import ir.co.ppaz.corebanking.codes.DefaultCodes;
import ir.co.ppaz.corebanking.core.CoreBankingException;
import ir.co.ppaz.corebanking.dto.UserDto;
import ir.co.ppaz.corebanking.model.Employee;
import ir.co.ppaz.corebanking.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping("/login")
    public ModelAndView login(HttpServletRequest servletRequest) {
        return new ModelAndView("home/login", new HashMap<String, Object>() {{
            put("userDto", new UserDto());
        }});
    }

    @PostMapping("/login")
    public ModelAndView login(@ModelAttribute UserDto userDto, HttpServletRequest servletRequest) {
        try {
            Employee employee = employeeService.login(userDto);
            servletRequest.getSession().setAttribute(DefaultCodes.USER_SESSION_NAME, employee);
            return new ModelAndView("redirect:/");
        } catch (CoreBankingException e) {
            return new ModelAndView("errors/error", new HashMap() {{
                put("code", e.getCode());
                put("message", e.getMessage());
            }});
        }
    }

    @GetMapping("/logout")
    public ModelAndView logout(HttpServletRequest servletRequest){
        servletRequest.getSession().removeAttribute(DefaultCodes.USER_SESSION_NAME);
        return new ModelAndView("redirect:/");
    }

}
