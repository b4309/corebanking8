package ir.co.ppaz.corebanking.controller;

import ir.co.ppaz.corebanking.codes.DefaultCodes;
import ir.co.ppaz.corebanking.core.CoreBankingException;
import ir.co.ppaz.corebanking.dto.PaymentDto;
import ir.co.ppaz.corebanking.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @GetMapping("/{depositId}")
    public ModelAndView list(@PathVariable Integer depositId, HttpServletRequest servletRequest){
        if(Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))){
            return new ModelAndView("redirect:/");
        }
        try {
            List<PaymentDto> depositPayments = paymentService.getDepositPayments(depositId);
            return new ModelAndView("payment/list", new HashMap<>(){{
                put("payments",depositPayments);
                put("depositId",depositId);
            }});
        }catch (CoreBankingException e) {
            return new ModelAndView("errors/error", new HashMap() {{
                put("code", e.getCode());
                put("message", e.getMessage());
            }});
        }
    }

    @GetMapping("/add/{depositId}")
    public ModelAndView add(@PathVariable Integer depositId, HttpServletRequest servletRequest) {
        if (Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))) {
            return new ModelAndView("redirect:/");
        }
        PaymentDto paymentDto = new PaymentDto();
        paymentDto.setDepositId(depositId);
        return new ModelAndView("payment/add", new HashMap<>(){{
            put("payment",paymentDto);
        }});
    }

    @PostMapping("/add")
    public ModelAndView add(@ModelAttribute PaymentDto paymentDto, HttpServletRequest servletRequest) {
        if (Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))) {
            return new ModelAndView("redirect:/");
        }
        try {
            paymentService.add(paymentDto);
            return new ModelAndView("redirect:/payment/"+paymentDto.getDepositId());
        }catch (CoreBankingException e) {
            return new ModelAndView("errors/error", new HashMap() {{
                put("code", e.getCode());
                put("message", e.getMessage());
            }});
        }
    }

}
