package ir.co.ppaz.corebanking.controller;

import ir.co.ppaz.corebanking.codes.DefaultCodes;
import ir.co.ppaz.corebanking.core.CoreBankingException;
import ir.co.ppaz.corebanking.dto.DepositCustomer;
import ir.co.ppaz.corebanking.model.Deposit;
import ir.co.ppaz.corebanking.service.CustomerService;
import ir.co.ppaz.corebanking.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Objects;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @Autowired
    DepositService depositService;

    @GetMapping("/showCustomerInfo/{customerId}")
    public ModelAndView showCustomerInfo(@PathVariable Integer customerId, HttpServletRequest servletRequest) {
        if (Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))) {
            return new ModelAndView("redirect:/");
        }
        throw new IllegalArgumentException();
    }

    @GetMapping("/showDepositInfo/{depositId}")
    public ModelAndView showDepositInfo(@PathVariable Integer depositId, HttpServletRequest servletRequest) {
        if (Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))) {
            return new ModelAndView("redirect:/");
        }
        try {
            Deposit deposit = depositService.getDepositById(depositId);
            return new ModelAndView("customer/showDepositInfo", new HashMap<>() {{
                put("customers", customerService.getDepositCustomers(deposit));
                put("deposit", deposit);
            }});
        } catch (CoreBankingException e) {
            return new ModelAndView("errors/error", new HashMap() {{
                put("code", e.getCode());
                put("message", e.getMessage());
            }});
        }
    }

    @GetMapping("/")
    public ModelAndView list(HttpServletRequest servletRequest) {
        if (Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))) {
            return new ModelAndView("redirect:/");
        }
        return new ModelAndView("customer/list", new HashMap<>() {{
            put("customers", customerService.getAllCustomers());
        }});
    }

    @GetMapping("/addRelation/{depositId}")
    public ModelAndView addRelation(@PathVariable Integer depositId, HttpServletRequest servletRequest) {
        if (Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))) {
            return new ModelAndView("redirect:/");
        }
        return new ModelAndView("customer/addRelation", new HashMap<>() {{
            put("depositId", depositId);
            put("customerList", customerService.getAllCustomersForRelation());
            put("customerRelation", DepositCustomer.builder().depositId(depositId).build());
        }});
    }

    @PostMapping("/addRelation")
    public ModelAndView addRelation(@ModelAttribute DepositCustomer depositCustomer, HttpServletRequest servletRequest) {
        if (Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))) {
            return new ModelAndView("redirect:/");
        }
        try {
            customerService.addRelation(depositCustomer);
            return new ModelAndView("redirect:/customer/showDepositInfo/" + depositCustomer.getDepositId());
        } catch (CoreBankingException e) {
            return new ModelAndView("errors/error", new HashMap() {{
                put("code", e.getCode());
                put("message", e.getMessage());
            }});
        }
    }

    @GetMapping("/addReal")
    public ModelAndView addReal(HttpServletRequest servletRequest) {
        if (Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))) {
            return new ModelAndView("redirect:/");
        }
        throw new IllegalArgumentException();
    }

    @GetMapping("/addLegal")
    public ModelAndView addLegal(HttpServletRequest servletRequest) {
        if (Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))) {
            return new ModelAndView("redirect:/");
        }
        throw new IllegalArgumentException();
    }

    @GetMapping("/edit/{customerId}")
    public ModelAndView edit(@PathVariable Integer customerId, HttpServletRequest servletRequest) {
        if (Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))) {
            return new ModelAndView("redirect:/");
        }
        throw new IllegalArgumentException();
    }

    @GetMapping("/delete/{customerId}")
    public ModelAndView delete(@PathVariable Integer customerId, HttpServletRequest servletRequest) {
        if (Objects.isNull(servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME))) {
            return new ModelAndView("redirect:/");
        }
        throw new IllegalArgumentException();
    }

}
