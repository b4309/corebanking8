package ir.co.ppaz.corebanking.controller;

import ir.co.ppaz.corebanking.codes.DefaultCodes;
import ir.co.ppaz.corebanking.model.Employee;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@RestController
@RequestMapping(value = "/")
public class HomeController {

    @GetMapping(value = "/")
    public ModelAndView home(HttpServletRequest servletRequest) {
        if (servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME) == null) {
            return new ModelAndView("redirect:/employee/login");
        } else {
            Employee employee = (Employee) servletRequest.getSession().getAttribute(DefaultCodes.USER_SESSION_NAME);
            return new ModelAndView("home/index", new HashMap(){{
                put("employee", employee);
            }});
        }
    }


}
