package ir.co.ppaz.corebanking.repository;

import ir.co.ppaz.corebanking.model.Deposit;
import ir.co.ppaz.corebanking.model.Payment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentRepository extends CrudRepository<Payment,Integer> {

    List<Payment> getPaymentsByDepositOrderByDate(Deposit deposit);

    @Query(value = "select * from payment p where p.deposit_id=?1 order by p.id desc limit 1", nativeQuery = true)
    Payment getLastPaymentByDeposit(Integer depositId);

}
