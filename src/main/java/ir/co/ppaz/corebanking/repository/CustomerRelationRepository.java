package ir.co.ppaz.corebanking.repository;

import ir.co.ppaz.corebanking.model.CustomerRelation;
import ir.co.ppaz.corebanking.model.Deposit;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRelationRepository extends CrudRepository<CustomerRelation,Integer> {

    List<CustomerRelation> findCustomerRelationByDeposit(Deposit deposit);

    @Query("select sum(r.portion) from CustomerRelation r where r.deposit.number=?1")
    Long sumPortionOfDeposit(String depositNumber);

}
