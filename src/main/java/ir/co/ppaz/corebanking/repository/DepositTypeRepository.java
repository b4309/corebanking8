package ir.co.ppaz.corebanking.repository;

import ir.co.ppaz.corebanking.model.Deposit;
import ir.co.ppaz.corebanking.model.DepositType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepositTypeRepository extends CrudRepository<DepositType,Integer> {

}
