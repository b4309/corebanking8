package ir.co.ppaz.corebanking.repository;

import ir.co.ppaz.corebanking.model.RealCustomer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RealCustomerRepository extends CrudRepository<RealCustomer,Integer> {
}
