package ir.co.ppaz.corebanking.repository;

import ir.co.ppaz.corebanking.model.LegalCustomer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LegalCustomerRepository extends CrudRepository<LegalCustomer,Integer> {
}
