package ir.co.ppaz.corebanking.repository;

import ir.co.ppaz.corebanking.model.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository<Customer,Integer> {

    @Query("SELECT count(c) from Customer c where c.isReal=false")
    Long countLegalCustomer();

}
