package ir.co.ppaz.corebanking.repository;

import ir.co.ppaz.corebanking.model.Deposit;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepositRepository extends CrudRepository<Deposit,Integer> {

    @Query(value = "SELECT d.number from deposit d  order by d.id desc limit 1", nativeQuery = true)
    String getLastNumber();

}
