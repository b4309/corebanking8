package ir.co.ppaz.corebanking.repository;

import ir.co.ppaz.corebanking.model.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee,Integer> {

    @Query("SELECT e from Employee e where e.username=?1 and e.password=?2")
    Employee login(String username, String password);

    Employee findEmployeeByUsernameAndPassword(String username, String password);

}
