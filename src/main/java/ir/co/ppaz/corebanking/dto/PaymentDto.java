package ir.co.ppaz.corebanking.dto;

import ir.co.ppaz.corebanking.enums.PaymentTypeEnum;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaymentDto {

    private Integer depositId;
    private Integer paymentId;
    private String date;
    private String time;
    private Long prevAmount;
    private Long amount;
    private Long newAmount;
    private PaymentTypeEnum type;


}
