package ir.co.ppaz.corebanking.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DepositCustomer {

    private Integer depositId;
    private Integer relationId;
    private Integer customerId;
    private String title;
    private Boolean isReal;
    private String nationalId;
    private Integer portion;

}
