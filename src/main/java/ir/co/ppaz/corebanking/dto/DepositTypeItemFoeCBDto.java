package ir.co.ppaz.corebanking.dto;

import ir.co.ppaz.corebanking.model.DepositType;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DepositTypeItemFoeCBDto {

    private Integer id;
    private String title;

    public DepositTypeItemFoeCBDto(DepositType depositType) {
        this.id = depositType.getId();
        this.title = depositType.getTitle();
    }
}
