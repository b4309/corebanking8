package ir.co.ppaz.corebanking.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DepositNewEditDto {

    private Integer id;
    private String number;
    private String title;
    private String openingDate;
    private String expDate;
    private Boolean isOpen;
    private Integer typeId;

}
